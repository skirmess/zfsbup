#!/usr/bin/perl
# vim: ts=4 sts=4 sw=4 et: syntax=perl

use 5.034;
use strict;
use warnings;

use feature 'try';
no warnings "experimental::try";

use Log::Log4perl qw(get_logger);
use MIME::Base64  qw(encode_base64);
use Path::Tiny;
use Sys::Hostname;

use lib::relative '../lib';

use Local::ZFSBup;
use Local::ZFSBup::System;

main();

sub main {
    my $server_name = lc hostname();

    # call new() to initialize logger
    my $zfsbup = Local::ZFSBup->new(
        config    => $server_name,
        lock_file => path(__FILE__)->absolute->parent(2)->child('zfsbup.run')->stringify,
        log_dir   => path(__FILE__)->absolute->parent(2)->child('log')->stringify,
    );

    #
    my $logger = get_logger();

    my $config    = $zfsbup->config;
    my $timestamp = $zfsbup->timestamp;

    $logger->info("Starting Backup $config $timestamp");

    my $mail = $zfsbup->initialize_report('sven.kirmess@kzone.ch');    # TODO from config

    try {
        # parse config
        my @hosts = ( lc 'adarak' );                                   # TODO from config

        my $backup_root_enc = "backup/enc";                            # TODO from config
        die "Invalid backup root name '$backup_root_enc'" if !Local::ZFSBup::Check->dataset($backup_root_enc);

        my $backup_root_raw = "backup/raw";                            # TODO from config
        die "Invalid backup root name '$backup_root_raw'" if !Local::ZFSBup::Check->dataset($backup_root_raw);

        # Obatain lock
        if ( !$zfsbup->obtain_lock ) {
            $logger->error("zfsbup already running");
            $logger->info("Aborting backup ...");

            $mail->zfsbup_already_running($config);
            exit 1;
        }

        $zfsbup->write_state("START");
        $zfsbup->write_state("CONFIG $config");
        $zfsbup->write_state("TIMESTAMP $timestamp");

        # Create System object for backup server
        my $server = Local::ZFSBup::System->new(
            backup => $zfsbup,
            name   => $server_name,
            local  => 1
        );

        # check if the backup root filesystem exist
        $logger->debug("Check if encrypted backup root filesystem '$backup_root_enc' exists");
        if ( !$server->zfs_dataset_exists( $backup_root_enc, 'filesystem' ) ) {
            $logger->error("backup root '$backup_root_enc' does not exist on backup server");
            $logger->info("Aborting backup ...");

            $mail->zfsbup_failed( $config, "Backup root '$backup_root_enc' does not exist" );
            exit 1;
        }

        $logger->debug("Check if raw backup root filesystem '$backup_root_raw' exists");
        if ( !$server->zfs_dataset_exists( $backup_root_raw, 'filesystem' ) ) {
            $logger->error("backup root '$backup_root_raw' does not exist on backup server");
            $logger->info("Aborting backup ...");

            $mail->zfsbup_failed( $config, "Backup root '$backup_root_raw' does not exist" );
            exit 1;
        }

        my $rc_backup = 0;
      HOST:
        for my $client_name (@hosts) {
            $logger->info("Starting backup of client '$client_name'");
            my $rc_client = 0;

            my $client;
            my @datasets;
            my $backup_root_client_enc = "$backup_root_enc/$client_name";
            my $backup_root_client_raw = "$backup_root_raw/$client_name";

            try {
                die "Invalid dataset name '$backup_root_client_enc'" if !Local::ZFSBup::Check->dataset($backup_root_client_enc);
                die "Invalid dataset name '$backup_root_client_raw'" if !Local::ZFSBup::Check->dataset($backup_root_client_raw);

                $client = Local::ZFSBup::System->new(
                    backup  => $zfsbup,
                    name    => $client_name,
                    ssh_key => '/home/admin/zfsbup/.ssh/id_ed25519',    # TODO
                );
                @datasets = $client->datasets();

                $logger->debug("Check if encrypted backup root '$backup_root_client_enc' for client '$client_name' exists");
                if ( !$server->zfs_dataset_exists( $backup_root_client_enc, 'filesystem' ) ) {
                    $logger->info("Creating new backup root for client '$client_name': $backup_root_client_enc");

                    die "Could not create filesystem '$backup_root_client_enc'" if !$server->zfs_create_backup_root_filesystem($backup_root_client_enc);
                }

                $logger->debug("Check if raw backup root '$backup_root_client_raw' for client '$client_name' exists");
                if ( !$server->zfs_dataset_exists( $backup_root_client_raw, 'filesystem' ) ) {
                    $logger->info("Creating new backup root for client '$client_name': $backup_root_client_raw");

                    die "Could not create filesystem '$backup_root_client_raw'" if !$server->zfs_create_backup_root_filesystem($backup_root_client_raw);
                }
            }
            catch ($e) {
                $logger->error($e);
                $logger->error("Backup of host '$client_name' failed");
                $rc_backup = 1;
                $zfsbup->write_backup_state( $client_name, 'FAILED' );
                next HOST;
            }

          DATASET:
            for my $dataset_ref ( sort @datasets ) {
                my ( $dataset_name, $guid, $encryption ) = @{$dataset_ref};

                $logger->debug("Starting backup of dataset '$dataset_name' (guid $guid) from client '$client_name'");

                my $backup_root_client_dataset;
                my $raw;
                if ( $encryption eq 'off' ) {

                    # data is not encrypted on client, transfer it to the enc dir to encrypt it here
                    $backup_root_client_dataset = "$backup_root_client_enc/" . _sven_encode("${dataset_name}-$guid");
                    $raw                        = 0;
                }
                else {
                    # data is encrypted on client, transfer it to raw
                    $backup_root_client_dataset = "$backup_root_client_raw/" . _sven_encode("${dataset_name}-$guid");
                    $raw                        = 1;
                }

                my $snapshots_transferred;
                try {
                    $snapshots_transferred = $server->backup( $client, $dataset_name, $backup_root_client_dataset, $raw );
                }
                catch ($e) {
                    $logger->error($e);
                }

                if ( !defined $snapshots_transferred ) {
                    $logger->error("Backup of dataset '$dataset_name' from client '$client_name' failed");
                    $rc_client = 1;
                    $zfsbup->write_backup_state( $client_name, 'FAILED', $dataset_name );
                    next DATASET;
                }

                $logger->info( "Dataset '$dataset_name' from client '$client_name' successfully backed up ($snapshots_transferred snapshot" . ( $snapshots_transferred != 1 ? 's' : q{} ) );

                # Mention successfully backed up snapshots to backup client
                # to allow for snapshot removal
                my $backed_up_snapshots = "NAME $dataset_name\nGUID $guid\n";
              SNAPSHOT:
                for my $snap ( $server->snapshots($backup_root_client_dataset) ) {
                    die "Invalid snapshot $snap" if $snap !~ m{ \A \Q$backup_root_client_dataset\E [@] ( .+ ) \z }xsm;
                    my $snap_name = $1;
                    next SNAPSHOT if $snap_name !~ m{ \A__backup__ [12][0-9][0-9][0-9] - [01][0-9] - [0-3] [0-9] \z }xsm;
                    $backed_up_snapshots .= "SNAP $snap_name\n";
                }
                $backed_up_snapshots .= "END";

                my $tmp_file = path(__FILE__)->parent(2)->child('.backed_up_snapshots.txt');
                $tmp_file->remove;
                $tmp_file->spew($backed_up_snapshots);

                $client->copy_backed_up_snapshots_file($tmp_file->stringify, "${server_name}.${guid}.txt");
            }

            my $msg = "Backup of host '$client_name' finished";
            if ( $rc_client != 0 ) {
                $rc_backup = 1;
                $msg .= ' with failures';
            }

            $logger->info($msg);
        }

        if ( $rc_backup == 0 ) {
            $zfsbup->write_state("END SUCCESS");
        }
        else {
            $zfsbup->write_state("END FAIL");
        }

        $zfsbup->release_lock;

        exit $rc_backup;
    }
    catch ($e) {
        $logger->error($e);

        $logger->info("Aborting backup ...");

        $mail->zfsbup_failed( $config, $e );
    }

    exit 1;
}

sub _sven_encode {
    my ($text) = @_;

    my $s1 = '';
    my $s2 = '';
    my @a  = split //, $text;
    for my $a (@a) {
        if ( $a =~ m{ ^ [a-zA-Z0-9._] $ }xsm ) {
            $s1 .= $a;
            next;
        }
        $s1 .= '-';
        $s2 .= $a;
    }

    if ( $s2 ne '' ) {
        $s2 = encode_base64($s2);
        $s2 =~ s{ \n }{}xsmg;
        $s2 =~ s{ [+] }{-}xsmg;
        $s2 =~ s{ / }{_}xsmg;
        $s2 =~ s{ = }{}xsmg;
        $s1 .= "-$s2";
    }

    return $s1;
}
