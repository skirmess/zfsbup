# vim: ts=4 sts=4 sw=4 et: syntax=perl

use 5.034;
use strict;
use warnings;

package Local::ZFSBup::System;
use Object::Pad;
class Local::ZFSBup::System;

no warnings 'qw';
use feature 'signatures';
use feature 'try';
no warnings "experimental::try";

use Carp;
use Log::Log4perl qw(get_logger);

use Local::ZFSBup::Check;

field $backup : param;
field $hostname : param = undef;
field $local : param    = 0;
field $name : param : reader;
field $ssh_key : param = undef;
field $user : param    = 'admin';

BUILD {
    if ( !$local && !defined $hostname ) {
        $hostname = $name;

        # check if the client name contains invalid characters
        croak "Client name '$hostname' is invalid" if !Local::ZFSBup::Check->hostname($hostname);
    }

    croak q{'ssh_key' is needed if not 'local'} if !$local && !defined $ssh_key;

    croak "User name '$user' is invalid" if !Local::ZFSBup::Check->user_name($user);
}

method copy_backed_up_snapshots_file ($src_file, $dst_file_name) {
    my $logger = get_logger();

    croak q{'ssh_key' not set} if !defined $ssh_key;

    my @cmd = ( 'scp', '-q', '-p', '-i', $ssh_key, $src_file, "$user\@$hostname:backup/$dst_file_name" );

    my $cmd = join( q{ }, @cmd );
    $logger->trace("Executing [$cmd]");

    my $rc = system(@cmd);

    croak "system failed: $!"                         if $rc == -1;
    croak "child died with signal " . ( $rc & 127 ) if $rc & 127;

    $rc = ( $rc >> 8 );

    $logger->trace("rc = $rc [$cmd]");

    return;
}

method create_command (@cmd) {
    return @cmd if $local;

    croak q{'ssh_key' not set} if !defined $ssh_key;

    my @remote_cmd = ( 'ssh', '-i', $ssh_key, '-l', $user, $hostname, @cmd );
    return @remote_cmd;
}

method datasets {
    my $logger = get_logger();

    my @cmd = $self->create_command(qw( zfs list -t filesystem,volume -o name,guid,ch.kzone:zfsbup,encryption -H));
    my $cmd = join( q{ }, @cmd );

    my ( $pipe_write, $pipe_read );
    pipe $pipe_read, $pipe_write or croak "Cannot create pipe: $!";

    my $pid = fork;
    croak "fork failed: $!" if !defined $pid;

    if ( $pid == 0 ) {

        # child
        close $pipe_read or croak "Cannot close pipe: $!";
        open STDERR, '>',  '/dev/null' or croak "Cannot redirect STDERR: $!";
        open STDOUT, '>&', $pipe_write or croak "Cannot redirect STDOUT: $!";
        close $pipe_write or croak "Cannot close pipe: $!";

        $logger->trace("Executing [$cmd]");
        exec(@cmd);
        die "exec failed: $!";
    }

    my @fs;

    close $pipe_write or croak "Cannot close pipe: $!";

  ZFS_LIST:
    while ( my $line = <$pipe_read> ) {
        chomp $line;
        my ( $dataset, $guid, $zfsbup, $encryption, $remainder ) = split( /\s+/, $line );

        croak "unexpected output from zfs list"                        if defined $remainder;
        croak "Unexpected guid of dataset $dataset: $guid"             if $guid       !~ m{ \A [1-9][0-9]* \z }xsm;
        croak "Unexpected encryption of dataset $dataset: $encryption" if $encryption !~ m{ \A (?: aes-256-gcm | off ) \z }xsm;

        if ( $zfsbup eq 'on' ) {
            push @fs, [ $dataset, $guid, $encryption ];
            $logger->debug("Dataset '$dataset' on host '$name' will be backed up");
            next ZFS_LIST;
        }

        if ( $zfsbup eq 'off' ) {
            $logger->debug("Skipping dataset '$dataset' from host '$name'");
            $backup->write_state("SKIP $name $dataset");
            next ZFS_LIST;
        }

        if ( $zfsbup eq '-' ) {
            $logger->warn("Property 'ch.kzone:zfsbup' not defined on dataset '$dataset' from host '$name'");
            $backup->write_state("SKIP $name $dataset");
            next ZFS_LIST;
        }

        $logger->error("Invalid value '$zfsbup' of property 'ch.kzone:zfsbup' on dataset '$dataset' on host '$name'");
    }

    close $pipe_read or croak "Cannot close pipe: $!";

    waitpid $pid, 0;
    my $rc = $?;

    croak "exec failed: $!"                         if $rc == -1;
    croak "child died with signal " . ( $rc & 127 ) if $rc & 127;

    $rc = ( $rc >> 8 );

    $logger->trace("rc = $rc [$cmd]");

    return @fs if $rc == 0;
    return;
}

method zfs_dataset_exists ( $dataset, $dataset_type ) {
    my $logger = get_logger();

    my %allowed_dataset_types = map { $_ => 1 } qw(
      filesystem
      volume
      filesystem,volume
      volume,filesystem
    );

    croak "internal error: invalid dataset type '$dataset_type'" if !defined $allowed_dataset_types{$dataset_type};

    my @cmd = $self->create_command( qw(zfs list -H -o name -t), $dataset_type, $dataset );
    my $cmd = join( " ", @cmd );

    my ( $pipe_write, $pipe_read );
    pipe $pipe_read, $pipe_write or croak "Cannot create pipe: $!";

    my $pid = fork;
    croak "fork failed: $!" if !defined $pid;

    if ( $pid == 0 ) {

        # child
        close $pipe_read or croak "Cannot close pipe: $!";
        open STDERR, '>',  '/dev/null' or croak "Cannot redirect STDERR: $!";
        open STDOUT, '>&', $pipe_write or croak "Cannot redirect STDOUT: $!";
        close $pipe_write or croak "Cannot close pipe: $!";

        $logger->trace("Executing [$cmd]");
        no warnings 'exec';
        exec(@cmd);
        croak "exec failed: $!";
    }

    close $pipe_write or croak "Cannot close pipe: $!";
    my $ds = <$pipe_read>;
    close $pipe_read or croak "Cannot close pipe: $!";

    waitpid $pid, 0;
    my $rc = $?;

    croak "exec failed: $!"                         if $rc == -1;
    croak "child died with signal " . ( $rc & 127 ) if $rc & 127;

    $rc = $rc >> 8;
    $logger->trace("rc = $rc [$cmd]");

    # non-zero return code
    return if $rc != 0;

    chomp $ds;
    croak "unexpected output from zfs list" if $ds ne $dataset;

    return 1;
}

method zfs_create_backup_root_filesystem ($filesystem) {
    my $logger = get_logger();

    croak "internal error: cannot create backup root on client." if !$local;

    my @cmd = $self->create_command( qw(pfexec zfs create -o readonly=on -o canmount=off -o mountpoint=legacy), $filesystem );
    my $cmd = join( " ", @cmd );

    $logger->trace("Executing [$cmd]");
    system(@cmd);
    my $rc = $?;

    croak "exec failed: $!"                         if $rc == -1;
    croak "child died with signal " . ( $rc & 127 ) if $rc & 127;

    $rc = $rc >> 8;
    $logger->trace("rc = $rc [$cmd]");

    # non-zero return code
    return if $rc != 0;

    return 1;
}

method snapshots ( $dataset, $type = undef ) {
    my $logger = get_logger();

    croak "Invalid dataset name '$dataset'" if !Local::ZFSBup::Check->dataset($dataset);
    my @cmd = $self->create_command( qw(zfs list -d 1 -t snapshot -o name,ch.kzone:zfs-snapshot-type -H), $dataset );
    my $cmd = join( " ", @cmd );

    my ( $pipe_write, $pipe_read );
    pipe $pipe_read, $pipe_write or croak "Cannot create pipe: $!";

    my $pid = fork;
    croak("fork failed: $!") if !defined $pid;

    if ( $pid == 0 ) {

        # child
        close $pipe_read or croak "Cannot close pipe: $!";
        open STDERR, '>',  '/dev/null' or croak "Cannot redirect STDERR: $!";
        open STDOUT, '>&', $pipe_write or croak "Cannot redirect STDOUT: $!";
        close $pipe_write or croak "Cannot close pipe: $!";

        $logger->trace("Executing [$cmd]");
        exec(@cmd);
        die "exec failed: $!";
    }

    close $pipe_write or croak "Cannot close pipe: $!";

    my @snapshots = <$pipe_read>;

    close $pipe_read or croak "Cannot close pipe: $!";

    waitpid $pid, 0;
    my $rc = $?;

    croak "exec failed: $!"                         if $rc == -1;
    croak "child died with signal " . ( $rc & 127 ) if $rc & 127;

    $rc = $rc >> 8;
    $logger->trace("rc = $rc [$cmd]");

    return if $rc != 0;

    my @result;
  SNAPSHOT:
    for my $snapshot (@snapshots) {
        chomp $snapshot;

        my ( $snap_name, $snap_type ) = split /\s/, $snapshot, 2;

        croak "Snapshot '$snap_name' is not in dataset '$dataset' on client '$hostname'" if $snap_name !~ m{ \A $dataset [@] .+ $ }xms;

        if ( defined $type ) {
            next SNAPSHOT if $snap_type ne $type;
        }

        push @result, $snap_name;
    }

    return @result;
}

method zfs_fetch ( $client, $raw, $snapshot, $target, $incremental_snapshot = undef ) {
    my $logger = get_logger();

    my @cmd_send = $client->create_command(qw(pfexec zfs send));
    if ($raw) {
        push @cmd_send, '-w';
    }
    if ( defined $incremental_snapshot ) {
        push @cmd_send, '-i', "\@$incremental_snapshot";
    }
    push @cmd_send, $snapshot;
    my $cmd_send = join( " ", @cmd_send );

    my @cmd_recv = $self->create_command( qw(pfexec zfs recv -u), $target );
    my $cmd_recv = join( " ", @cmd_recv );

    my ( $pipe_write, $pipe_read );
    pipe $pipe_read, $pipe_write or croak "Cannot create pipe: $!";

    my $pid_recv = fork;
    croak "fork failed: $!" if !defined $pid_recv;

    if ( $pid_recv == 0 ) {

        # child / recv
        close $pipe_write or croak("Cannot close pipe: $!");
        open STDERR, '>',  '/dev/null' or croak "Cannot redirect STDERR: $!";
        open STDOUT, '>',  '/dev/null' or croak "Cannot redirect STDOUT: $!";
        open STDIN,  '<&', $pipe_read  or croak "Cannot redirect STDIN: $!";
        close $pipe_read or croak "Cannot close pipe: $!";

        $logger->trace("Executing [$cmd_recv]");
        exec(@cmd_recv);
        die "exec failed: $!";
    }

    close $pipe_read or croak "Cannot close pipe: $!";

    my $pid_send = fork;
    croak "fork failed: $!" if !defined $pid_send;

    if ( $pid_send == 0 ) {

        # child / send
        open STDERR, '>',  '/dev/null' or croak "Cannot redirect STDERR: $!";
        open STDOUT, '>&', $pipe_write or croak "Cannot redirect STDOUT: $!";
        close $pipe_write or croak "Cannot close pipe: $!";

        $logger->trace("Executing [$cmd_send]");
        exec(@cmd_send);
        die "exec failed: $!";
    }

    close $pipe_write or croak "Cannot close pipe: $!";

    waitpid $pid_send, 0;
    my $rc_send = $?;

    waitpid $pid_recv, 0;
    my $rc_recv = $?;

    croak "exec failed: $!"                              if $rc_send == -1;
    croak "child died with signal " . ( $rc_send & 127 ) if $rc_send & 127;

    $rc_send = $rc_send >> 8;
    $logger->trace("rc = $rc_send [$cmd_send]");

    croak "exec failed: $!"                              if $rc_recv == -1;
    croak "child died with signal " . ( $rc_recv & 127 ) if $rc_recv & 127;

    $rc_recv = $rc_recv >> 8;
    $logger->trace("rc = $rc_recv [$cmd_recv]");

    # non-zero return code
    croak "send failed" if $rc_send != 0;
    croak "recv failed" if $rc_recv != 0;

    return 1;
}

method zfs_get ( $filesystem, $property ) {
    my $logger = get_logger();

    my @cmd = $self->create_command( qw(zfs get -Hp), $property, $filesystem );
    my $cmd = join( " ", @cmd );

    my ( $pipe_write, $pipe_read );
    pipe $pipe_read, $pipe_write or croak "Cannot create pipe: $!";

    my $pid = fork;
    croak "fork failed: $!" if !defined $pid;

    if ( $pid == 0 ) {

        # child
        close $pipe_read or croak "Cannot close pipe: $!";
        open STDERR, '>',  '/dev/null' or croak "Cannot redirect STDERR: $!";
        open STDOUT, '>&', $pipe_write or croak "Cannot redirect STDOUT: $!";
        close $pipe_write or croak "Cannot close pipe: $!";

        $logger->trace("Executing [$cmd]");
        no warnings 'exec';
        exec(@cmd);
        croak "exec failed: $!";
    }

    close $pipe_write or croak "Cannot close pipe: $!";
    my $data = <$pipe_read>;
    close $pipe_read or croak "Cannot close pipe: $!";

    waitpid $pid, 0;
    my $rc = $?;

    croak "exec failed: $!"                         if $rc == -1;
    croak "child died with signal " . ( $rc & 127 ) if $rc & 127;

    $rc = $rc >> 8;
    $logger->trace("rc = $rc [$cmd]");

    # non-zero return code
    return if $rc != 0;

    croak "Cannot parse output from zfs get: $data" if $data !~ m{ \A \Q$filesystem\E \s+ $property \s+ ( [a-z]+ ) \s }xsm;
    my $type = $1;

    return $type;
}

method zfs_set ( $filesystem, $property ) {
    my $logger = get_logger();

    my @cmd = $self->create_command( qw(pfexec zfs set), $property, $filesystem );
    my $cmd = join( " ", @cmd );

    $logger->trace("Executing [$cmd]");
    system(@cmd);
    my $rc = $?;

    croak "exec failed: $!"                         if $rc == -1;
    croak "child died with signal " . ( $rc & 127 ) if $rc & 127;

    $rc = $rc >> 8;
    $logger->trace("rc = $rc [$cmd]");

    return if $rc != 0;
    return 1;
}

method get_backup_size ( $dataset, $first_backup_snapshot, $second_backup_snapshot = undef ) {

    # used	  @ dataset  -> total size
    # refer	  @ snapshot -> amount of data accessible through this snapshot
    # written @ snapshot -> amount of new data written for this snapshot

    # If $second_backup_snapshot is undef, returns the size of the
    # $first_backup_snapshot. This will be used for initial
    # transfers
    #
    # If the $second_backup_snapshot is defined, returns the size of the
    # sum of all snapshots after (but not including!) the
    # $first_backup_snapshot. This is for incremental tramsfers.

    my $logger = get_logger();

    # TODO check that snapshots are on dataset
    # TODO check that snapshots and dataset are valid
    $first_backup_snapshot = $dataset . '@' . $first_backup_snapshot;
    if ( defined $second_backup_snapshot ) {
        $second_backup_snapshot = $dataset . '@' . $second_backup_snapshot;
    }

    my @cmd = $self->create_command( qw(zfs list -d 1 -t snapshot -o name,written -H -p), $dataset );
    my $cmd = join( " ", @cmd );

    my ( $pipe_write, $pipe_read );
    pipe $pipe_read, $pipe_write or croak "Cannot create pipe: $!";

    my $pid = fork;
    croak("fork failed: $!") if !defined $pid;

    if ( $pid == 0 ) {

        # child
        close $pipe_read or croak("Cannot close pipe: $!");
        open STDERR, '>',  '/dev/null' or croak "Cannot redirect STDERR: $!";
        open STDOUT, '>&', $pipe_write or croak "Cannot redirect STDOUT: $!";
        close $pipe_write or croak "Cannot close pipe: $!";

        $logger->trace("Executing [$cmd]");
        exec(@cmd);
        die "exec failed: $!";
    }

    close $pipe_write or croak "Cannot close pipe: $!";

    # slurp
    chomp( my @snapshots = <$pipe_read> );

    my $written_sum          = 0;
    my $first_snapshot_found = 0;
    my $this_snapshot_found  = 0;

  SNAPSHOT:
    for my $line (@snapshots) {
        my ( $snapshot_in_output, $written, $remainder ) = split( /\t/, $line );

        croak "unexpected output from '$cmd'" if defined $remainder;

        if ( $first_snapshot_found == 0 ) {
            if ( $snapshot_in_output eq $first_backup_snapshot ) {
                $first_snapshot_found = 1;

                if ( !defined $second_backup_snapshot ) {

                    # $second_backup_snapshot is not defined, which means
                    # we are only interested in the size of this specific snapshot
                    $written_sum         = $written;
                    $this_snapshot_found = 1;
                    last SNAPSHOT;
                }

                # the size of the snapshot of the last backup is not of interest to us
                # therefore, next snapshot
                next SNAPSHOT;
            }

            next SNAPSHOT;
        }

        $written_sum += $written;

        if ( $snapshot_in_output eq $second_backup_snapshot ) {
            $this_snapshot_found = 1;
            last SNAPSHOT;
        }

    }
    close $pipe_read or croak "Cannot close pipe: $!";

    waitpid $pid, 0;
    my $rc = $?;

    croak "exec failed: $!"                         if $rc == -1;
    croak "child died with signal " . ( $rc & 127 ) if $rc & 127;

    $rc = $rc >> 8;
    $logger->trace("rc = $rc [$cmd]");

    croak "zfs list failed" if $rc != 0;

    croak "Could not find the snapshot generated by this backup" if $this_snapshot_found == 0;

    return $written_sum;
}

method backup ( $client, $client_dataset, $backup_dataset, $raw ) {
    my @remote_snapshots = $client->snapshots( $client_dataset, 'backup' );

    my $logger      = get_logger();
    my $client_name = $client->name;

    if ( !@remote_snapshots ) {
        $logger->info("No backup snapshots on $client_dataset on $client_name");
        return;
    }

    my $epoc                  = time;
    my $snapshots_transferred = 0;

    $logger->debug("Check if dataset '$backup_dataset' was backed up before.");
    if ( !$self->zfs_dataset_exists( $backup_dataset, 'filesystem,volume' ) ) {
        $logger->info("Initial backup of '$client_dataset' from client '$client_name'");

        my $first_snapshot_client = $remote_snapshots[0];
        $logger->info("'$first_snapshot_client' is oldest backup snapshot on dataset '$client_dataset' on client '$client_name'");

        $logger->info("Initial backup from snapshot '$first_snapshot_client' from client '$client_name'");
        try {
            $self->zfs_fetch( $client, $raw, $first_snapshot_client, $backup_dataset );
            $snapshots_transferred++;
        }
        catch ($e) {
            $logger->error($e);
            croak("Transfer of '$first_snapshot_client' from client '$client_name' to '$backup_dataset' failed");
        }

        my $fs_type = $self->zfs_get( $backup_dataset, 'type' );
        my @to_set;
        push @to_set, 'readonly=on';
        if ( $fs_type ne 'volume' ) {
            push @to_set, "canmount=off", "mountpoint=legacy";
        }
        for my $property (@to_set) {
            try {
                $self->zfs_set( $backup_dataset, $property );
            }
            catch ($e) {
                $logger->error($e);
                croak("Cannot set property '$property' on '$backup_dataset'");
            }
        }

        $logger->debug("Get size of initial backup of snapshot '$first_snapshot_client' from client '$client_name'");

        my $first_snapshot_client_naked = $first_snapshot_client;
        $first_snapshot_client_naked =~ s{ \A [^@]+ [@] }{}xsm;
        my $backup_size;
        try {
            $backup_size = $self->get_backup_size( $backup_dataset, $first_snapshot_client_naked );
        }
        catch ($e) {
            $logger->error($e);
        }

        $logger->info("Initial backup size: $backup_size bytes");

        $epoc = time - $epoc;

        $backup->write_backup_state( $client_name, 'INITIAL', $client_dataset, $epoc, $backup_size );

        return $snapshots_transferred if @remote_snapshots == 1;
    }

    # incremental backup
    $epoc = time;

    my @backup_server_snapshots       = $self->snapshots($backup_dataset);
    my @latest_backup_server_snapshot = split /[@]/, $backup_server_snapshots[-1];
    croak "Cannot parse $backup_server_snapshots[-1]" if @latest_backup_server_snapshot != 2;
    my $latest_backup_server_snapshot = $latest_backup_server_snapshot[1];

  SNAPSHOT:
    while ( defined( my $snap = shift @remote_snapshots ) ) {
        my @c = split /[@]/, $snap;
        croak "Cannot parse $snap" if @c != 2;

        last SNAPSHOT if $c[1] eq $latest_backup_server_snapshot;
    }

    # nothing to do
    return $snapshots_transferred if !@remote_snapshots;

    my $latest_backup_snapshot_on_server  = $latest_backup_server_snapshot;
    my $initial_snap_for_size_calculation = $latest_backup_snapshot_on_server;
    for my $remote_snapshot (@remote_snapshots) {
        my @s = split /[@]/, $remote_snapshot;
        croak "Cannot parse $remote_snapshot" if @s != 2;

        my $snapshot_to_transfer = $s[1];

        $logger->info("Incremental backup from snapshot '$latest_backup_snapshot_on_server' to '$snapshot_to_transfer' from client '$client_name'");

        try {
            $self->zfs_fetch( $client, $raw, "$client_dataset\@$snapshot_to_transfer", $backup_dataset, $latest_backup_snapshot_on_server );
            $snapshots_transferred++;
        }
        catch ($e) {
            croak("Incremental transfer from '$latest_backup_snapshot_on_server' to '$snapshot_to_transfer' from client '$client_name' to '$backup_dataset' failed");
        }

        $logger->info("Incremental transfer from '$latest_backup_snapshot_on_server' to '$snapshot_to_transfer' from client '$client_name' to '$backup_dataset' successful");

        $latest_backup_snapshot_on_server = $snapshot_to_transfer;
    }

    $logger->debug("Get size of incremental backup");
    my $backup_size;
    try {
        $backup_size = $self->get_backup_size( $backup_dataset, $initial_snap_for_size_calculation, $latest_backup_snapshot_on_server );
    }
    catch ($e) {
        $logger->error($e);
    }

    $logger->info("Incremental backup size: $backup_size bytes");

    $epoc = time - $epoc;

    $backup->write_backup_state( $client_name, 'SUCCESS', $client_dataset, $epoc, $backup_size );

    # TODO create snap file

    return $snapshots_transferred;
}

1;
