# vim: ts=4 sts=4 sw=4 et: syntax=perl

use 5.034;
use strict;
use warnings;

package Local::ZFSBup::Report;
use Object::Pad;
class Local::ZFSBup::Report;

use feature 'signatures';

use Carp;
use Log::Log4perl qw(get_logger);
use Sys::Hostname;

field $html             = 1;
field $to : param       = 'root';
field $from : param     = undef;
field $sendmail : param = 'sendmail';

BUILD {

    # https://rt.cpan.org/Public/Bug/Display.html?id=18747
    # local $Carp::CarpLevel = $Carp::CarpLevel + 1;

    my $logger = get_logger();

    $logger->debug("Email reports will be mailed to '$to'");
    if ( defined $from ) {
        $logger->debug("Email reports will be mailed from '$from'");
    }
    $logger->debug("Email reports will be mailed with '$sendmail'");
}

method send_mail ( $subject, @body ) {
    croak "No subject for mail report defined" if !defined $subject;

    my @sendmail_cmd = ( $sendmail, '-i', '-t' );

    open my $fh, '|-', @sendmail_cmd or croak "Cannot run $sendmail: $!";

    if ( defined $from ) {
        print $fh "From: $from\n";
    }

    print $fh "To: $to\n";
    print $fh "Subject: $subject\n";

    if ($html) {
        print $fh "MIME-Version: 1.0\n";
        print $fh "Content-Type: text/html\n";
        print $fh "Content-Disposition: inline\n";
    }

    print $fh "\n";

    if ($html) {
        print $fh "<html>\n";
        print $fh "<body>\n";
        print $fh '<pre style="font-family: Consolas, Courier New, Courier, monospace; font-size: 14px">' . "\n";
    }

    for my $line (@body) {
        chomp $line;

        print $fh $line, "\n";
    }

    if ($html) {
        print $fh "</pre>\n";
        print $fh "</body>\n";
        print $fh "</html>\n";
    }

    close $fh or croak "Could not send mail: $!";

    return;
}

method zfsbup_already_running ($config) {
    croak "internal error: config not defined" if !defined $config;

    my $subject = "$config FATAL: ZFSBUP ALREADY RUNNING";
    my $body    = "zfsbup '$config' is already running";

    $self->send_mail( $subject, $body );

    return;
}

method zfsbup_failed ( $config, $msg ) {
    croak "internal error: config not defined" if !defined $config;

    my $subject = "$config FATAL: ZFSBUP FAILED";

    $self->send_mail( $subject, $msg );

    return;
}

sub _pretty_bytes {
    my ( $self, $size ) = @_;

    foreach my $unit ( 'B', 'kB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB' ) {
        if ( $size < 1000 ) {
            $size = int( $size + 0.5 );
            return sprintf( "%i %s", $size, $unit );
        }

        $size /= 1000;
    }

    # If we're here $size is bigger then 1000 ZB.
    # It was already divided by 1000 once again, which means we have
    # Yotta Bytes now, the biggest known prefix to me.
    return sprintf( "%i %s", $size, 'YB' );
}

method create_and_send_report (@state) {
    my $logger = get_logger();

    my @errors;
    my @summary;
    my @notes;

    # FAIL
    # STRANGE
    # ABORT
    #

    my $state_of_run = '';

    # START
    if ( shift(@state) ne 'START' ) {
        $logger->error("State file does not start with 'START'");
        $logger->error("Cannot generate report");
        return;
    }

    # CONFIG
    if ( @state == 0 ) {
        $logger->error("State file does not contain a config name");
        $logger->error("Cannot generate report");
        return;
    }

    my $config = shift @state;

    if ( $config !~ s{ ^ CONFIG \s+ ([a-z][a-z0-9]*) $ }{$1}xsm ) {
        $logger->error("State file does not contain a valid config name");
        $logger->error("Cannot generate report");
        return;
    }

    # TIMESTAMP
    if ( @state == 0 ) {
        $logger->error("State file does not contain a timestamp");
        $logger->error("Cannot generate report");
        return;
    }

    my $timestamp = shift @state;

    if ( $timestamp !~ s{ ^ TIMESTAMP \s+ (\d{4}-\d{2}-\d{2} - \d{6} Z) $}{$1}xsm ) {
        $logger->error("State file does not contain a valid timestamp");
        $logger->error("Cannot generate report");
        return;
    }

    # END
    if ( ( @state == 0 ) or ( $state[-1] !~ m{ ^ END \s }xsm ) ) {
        my $msg = "Last run did not finish correctly";
        $logger->info($msg);
        push @errors, $msg;
        $state_of_run = 'ABORT: ';
    }
    else {
        my $state = pop @state;
        if ( $state eq 'END SUCCESS' ) {

            # do nothing
        }
        elsif ( my ($status) = $state =~ m{ ^ END \s+ (FAIL|STRANGE) $ }xsm ) {
            $state_of_run = "$status: ";
        }
        else {
            $logger->error("Invalid success status in state file");
            $state_of_run = 'UNKNOWN: ';
        }
    }

  STATE:
    for my $state (@state) {
        my @a = split( /\s+/, $state );

        if ( $a[0] eq 'BACKUP' ) {
            my ( $action, $host, $status, $dataset, $duration, $size ) = @a;

            if ( $status eq 'FAILED' ) {
                if ( !defined $dataset ) {
                    push @errors, "$host $status";
                    next STATE;
                }

                push @errors, "$host $dataset $status";
                next STATE;
            }
            elsif ( $status eq 'SUCCESS' ) {
                my $minutes;
                {
                    use integer;
                    $minutes = $duration / 60;
                }
                my $seconds = $duration - $minutes * 60;

                $size = $self->_pretty_bytes($size);

                my $ds_len                 = length($dataset);
                my $dataset_printable_name = $ds_len > 47 ? substr( $dataset, $ds_len - 47, 47 ) : $dataset;

                push @summary, sprintf( "%-12s %-47s  %7s  %3i:%02i", $host, $dataset_printable_name, $size, $minutes, $seconds );
                next STATE;
            }
            elsif ( $status eq 'INITIAL' ) {
                push @notes, "Added new dataset: $host $dataset";

                my $minutes;
                {
                    use integer;
                    $minutes = $duration / 60;
                }
                my $seconds = $duration - $minutes * 60;

                $size = $self->_pretty_bytes($size);

                my $ds_len                 = length($dataset);
                my $dataset_printable_name = $ds_len > 47 ? substr( $dataset, $ds_len - 47, 47 ) : $dataset;

                push @summary, sprintf( "%-12s %-47s  %7s  %3i:%02i", $host, $dataset_printable_name, $size, $minutes, $seconds );

                next STATE;
            }
        }
        elsif ( $a[0] eq 'SKIP' ) {
            my ( $action, $host, $dataset ) = @a;
            push @notes, "Ignored dataset: $host $dataset";

            next STATE;
        }

        $logger->error("Unknown entry in state file '$state'");
    }

    # kzone.ch AMANDA MAIL REPORT FOR April 15, 2011
    # kzone.ch FAIL: AMANDA MAIL REPORT FOR April 22, 2011
    my $subject = "$config ${state_of_run}ZFSBUP MAIL REPORT FOR $timestamp";

    my @body = ( 'Hostname  : ' . hostname(), "Config    : $config", "Timestamp : $timestamp", '', );

    if (@errors) {
        push @body, 'FAILURES:', map { "  $_" } @errors, '';
    }

    if (@notes) {
        my @sorted_notes = sort @notes;
        push @body, 'NOTES:', map { "  $_" } @sorted_notes, '';
    }

    if (@summary) {
        push @body, 'BACKUP SUMMARY:', 'HOST         DATASET                                          WRITTEN  MMM:SS', '------------ -----------------------------------------------  -------  ------', @summary, '';
    }

    $self->send_mail( $subject, @body );

    return;
}

1;
