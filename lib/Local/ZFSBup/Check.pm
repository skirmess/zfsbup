# vim: ts=4 sts=4 sw=4 et: syntax=perl

use 5.034;
use strict;
use warnings;

package Local::ZFSBup::Check;

use feature 'signatures';

sub hostname ( $class, $name ) {
    return 1 if $name =~ m{ \A [a-z_-]+ (?: [.] [a-z]_-]+ )* \z }xsm;
    return;
}

sub config_name ( $class, $config ) {
    return 1 if $config =~ m{ \A [a-z]+ \z }xsm;
    return;
}

# https://docs.oracle.com/cd/E26505_01/html/E37384/gbcpt.html
sub dataset ( $class, $dataset ) {
    my $ZFS_COMPONENT_NAME = qr{ [a-zA-Z0-9][a-zA-Z0-9_:.-]* }xms;
    my $ZFS_POOL_NAME      = qr{ [a-zA-Z][a-zA-Z0-9_.-]* }xms;

    return 1 if $dataset =~ m{
        \A
        $ZFS_POOL_NAME
        (?:
            /
            $ZFS_COMPONENT_NAME
        )*
        \z
    }xsm;

    return;
}

sub email ( $class, $mail ) {

    # dirty and wrong, but good enough for me
    return 1 if $mail =~ m{ \A [a-zA-Z0-9._-]+ [@] [a-zA-Z0-9_-]+ (?: [.] [a-zA-Z0-9_-]+ )* \z }xsm;
    return;
}

sub user_name ( $class, $name ) {
    return 1 if $name =~ m{ \A [a-z]+ \z }xsm;
    return;
}

1;
