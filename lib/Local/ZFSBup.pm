# vim: ts=4 sts=4 sw=4 et: syntax=perl

use 5.034;
use strict;
use warnings;

package Local::ZFSBup;
use Object::Pad;
class Local::ZFSBup;

use feature 'signatures';

use Carp;
use Fcntl qw(LOCK_EX LOCK_NB SEEK_SET);
use File::stat;
use IO::Handle;
use Log::Log4perl qw(get_logger);
use Path::Tiny;

use Local::ZFSBup::Check;
use Local::ZFSBup::Report;

field $config : param : reader;
field $lock_file : param;
field $log_dir : param;

field $lock_handle;
field $report;
field $timestamp : reader;

BUILD {
    croak "log_dir '$log_dir' does not exist" if !-d $log_dir;

    croak "config name must only consist of lower case letters a-z but is '$config'" if !Local::ZFSBup::Check->config_name($config);

    path($lock_file)->touch;

    # Generate timestamp for backup
    my ( $sec, $min, $hour, $mday, $mon, $year ) = ( gmtime(time) )[ 0 .. 5 ];
    $timestamp = sprintf "%04s-%02s-%02s-%02s%02s%02sZ", ( $year + 1900 ), ( $mon + 1 ), $mday, $hour, $min, $sec;

    # Initialize log4perl
    my $conf = <<"LOG4PERL";
        log4perl.logger                    = TRACE, FileApp
        log4perl.appender.FileApp          = Log::Log4perl::Appender::File
        log4perl.appender.FileApp.mode     = append
        log4perl.appender.FileApp.syswrite = 1
        log4perl.appender.FileApp.filename = $log_dir/zfsbup_${config}_$timestamp.log
        log4perl.appender.FileApp.layout   = PatternLayout
        log4perl.appender.FileApp.layout.ConversionPattern = %d{yyyy-MM-dd HH:mm:ss}: %H: %-7p: %m%n
LOG4PERL

    Log::Log4perl->init( \$conf );
}

method initialize_report ($rcpt) {
    croak "Not a valid email address '$rcpt'" if !Local::ZFSBup::Check->email($rcpt);

    $report = Local::ZFSBup::Report->new( to => $rcpt );
    return $report;
}

method obtain_lock {
    croak 'internal error: already locked' if defined $lock_handle;

    open $lock_handle, '+>>', $lock_file or croak "Cannot open lock file '$lock_file': $!";

    flock( $lock_handle, LOCK_EX | LOCK_NB ) or return;

    $lock_handle->autoflush();

    my $st        = stat($lock_handle) or croak "Cannot stat lock file '$lock_file': $!";
    my $file_size = $st->size;

    if ( $file_size > 0 ) {
        croak "internal error: mail report not configured." if !defined $report;

        seek $lock_handle, 0, SEEK_SET or croak "Cannot read lock file '$lock_file': $!";

        chomp( my @state_file = <$lock_handle> );

        $report->create_and_send_report(@state_file);

        truncate $lock_handle, 0 or croak "Cannot write to state file '$lock_file': $!";
    }

    return 1;
}

method release_lock {
    my $logger => get_logger();

    croak "internal error: mail report not configured" if !defined $report;

    seek $lock_handle, 0, SEEK_SET or croak "Cannot read lock file '$lock_file': $!";

    chomp( my @state_file = <$lock_handle> );

    close $lock_handle or $logger->error("Could not finish writing state file '$lock_file': $!");

    $report->create_and_send_report(@state_file);

    unlink $lock_file or $logger->error("Could not remove lock file '$lock_file': $!");

    return;
}

method write_state ($msg) {
    croak "Internal error: no msg specified" if !defined $msg;

    croak "Lock was not obtained" if !defined $lock_handle;

    say {$lock_handle} $msg or croak "Could not write state to file '$lock_file': $!";

    $lock_handle->flush or croak "Could not flush state to file '$lock_file': $!";

    return;
}

method write_backup_state (@backup) {
    my $msg = "BACKUP " . join ' ', @backup;
    $self->write_state($msg);

    return;
}

1;
